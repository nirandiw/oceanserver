<?php
/* 
 * Abstract DAO class
 */
abstract class DAO {
    public function __construct() {
        $this->connect();
    }

    private function connect() {
        global $config;
        $this->con = mysql_connect($config["DB_HOST"], $config["DB_USER"], $config["DB_PASSWORD"],$config["DB_NAME"] );
        if (!$this->con) {
            throw new Exception("Failed to connect to database." . mysql_error());
        }
    }
    
    public function insert($sql) {
        $this->query($sql);
        $fileId = mysql_insert_id();
        return $fileId;
    }

    public function update($sql) {
        $this->query($sql);
    }

    public function query($sql) {
        global $config;
        mysql_select_db($config["DB_NAME"], $this->con);
        $result = mysql_query($sql, $this->con);
        if (!$result) {
            throw new Exception("Failed to execute query on database XXX." . mysql_error());
        }
        return $result;
    }
}
?>
