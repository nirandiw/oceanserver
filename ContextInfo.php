<?php
/**
 * Created by IntelliJ IDEA.
 * User: workshop
 * Date: 4/12/2014
 * Time: 5:50 PM
 */

class ContextInfo{
    private $contextInfoId;
    private $configGraphID;
    private $contextInfo;
    private $intent;

    /**
     * @return mixed
     */
    public function getConfigGraphID()
    {
        return $this->configGraphID;
    }

    /**
     * @param mixed $contextGraphID
     */
    public function setConfigGraphID($contextGraphID)
    {
        $this->configGraphID = $contextGraphID;
    }

    /**
     * @return mixed
     */
    public function getContextInfo()
    {
        return $this->contextInfo;
    }

    /**
     * @param mixed $contextInfo
     */
    public function setContextInfo($contextInfo)
    {
        $this->contextInfo = $contextInfo;
    }

    /**
     * @return mixed
     */
    public function getContextInfoId()
    {
        return $this->contextInfoId;
    }

    /**
     * @param mixed $contextInfoId
     */
    public function setContextInfoId($contextInfoId)
    {
        $this->contextInfoId = $contextInfoId;
    }

    /**
     * @return mixed
     */
    public function getIntent()
    {
        return $this->intent;
    }

    /**
     * @param mixed $intent
     */
    public function setIntent($intent)
    {
        $this->intent = $intent;
    }



}

?>