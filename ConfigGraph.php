<?php
/**
 * Created by IntelliJ IDEA.
 * User: workshop
 * Date: 3/12/2014
 * Time: 5:05 PM
 */
class ConfigGraph {
    private $configGraphID;
    private $isNew;
    private $featureSet;
    private $responceSet;

    /**
     * @return mixed
     */
    public function getConfigGraphID()
    {
        return $this->configGraphID;
    }

    /**
     * @param mixed $configGraphID
     */
    public function setConfigGraphID($configGraphID)
    {
        $this->configGraphID = $configGraphID;
    }

    /**
     * @return mixed
     */
    public function getFeatureSet()
    {
        return $this->featureSet;
    }

    /**
     * @param mixed $featureSet
     */
    public function setFeatureSet($featureSet)
    {
        $this->featureSet = $featureSet;
    }

    /**
     * @return mixed
     */
    public function getIsNew()
    {
        return $this->isNew;
    }

    /**
     * @param mixed $isNew
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;
    }

    /**
     * @return mixed
     */
    public function getResponceSet()
    {
        return $this->responceSet;
    }

    /**
     * @param mixed $responceSet
     */
    public function setResponceSet($responceSet)
    {
        $this->responceSet = $responceSet;
    }

}

?>