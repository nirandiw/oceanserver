CREATE TABLE a0106250_14_db.Video_File (
file_id int NOT NULL AUTO_INCREMENT, 
file_name varchar(100) NOT NULL,
dir_name varchar(100),
status int,
upload_start_time DATETIME,
upload_end_time DATETIME,
PRIMARY KEY(file_id)
);

CREATE TABLE a0106250_14_db.Streamlet (
streamlet_id int NOT NULL AUTO_INCREMENT, 
streamlet_name varchar(100) NOT NULL,
streamlet_no int NOT NULL,
file_id int NOT NULL,
PRIMARY KEY(streamlet_id),
FOREIGN KEY (file_id) REFERENCES Video_File(file_id) ON DELETE CASCADE
);
