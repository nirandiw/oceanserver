<?php
/**
 * Created by IntelliJ IDEA.
 * User: workshop
 * Date: 3/12/2014
 * Time: 4:48 PM
 */

class ConfigGraphsDAO extends DAO{
    public function insertFile($configGraph) {
        $sql = "INSERT INTO configgraphs(IS_NEW)" .
            "VALUES('{$configGraph->getIsNew()}')";
        return $this->insert($sql);
    }

    public function updateUploadStatus($fileId, $uploadStatus, $uploadEndTime) { //TODO:Implement this when I want to update the status
        $sql = "UPDATE Video_File SET status = $uploadStatus, upload_end_time = '$uploadEndTime' " .
            "WHERE file_id = $fileId";
        $this->update($sql);
    }

    public function findById($fileId) {
        $sql = "SELECT * FROM configgraphs WHERE CONFIG_GRAPH_ID = $fileId";
        $result = $this->query($sql);
        $rowCount = mysql_num_rows($result);

        if($rowCount > 0) {
            $row = mysql_fetch_array($result);
            return $this->getConfigGraph($row);
        }
    }
    public function findAll() {
        $sql = "SELECT * FROM configgraphs";
        $result = $this->query($sql);
        $fileList = array();
        while($row = mysql_fetch_array($result)) {
            $videoFile = $this->getConfigGraph($row);
            array_push($fileList, $videoFile);
        }
        return $fileList;
    }

    private function getConfigGraph($row) {
        $file = new ConfigGraph();
        $file->getConfigGraphID($row[CONFIG_GRAPH_ID]);
        $file->getIsNew($row[IS_NEW]);
        $file->getFeatureSet($row[FEATURE_SET]);
        $file->getResponceSet($row[RESPONSE_SET]);
        return $file;
    }
}

?>