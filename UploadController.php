<?php

require_once 'config.php';
require_once 'DAO.php';
require_once 'ConfigGraphsDAO.php';
require_once 'ConfigGraph.php';
require_once 'ContextInfo.php';
require_once 'ContextInfoDAO.php';




class UploadController
{

    private static $ACTION_UPLOAD_REQUEST = "uploadRequest";
    private static $ACTION_UPLOAD = "upload";
    private static $ACTION_UPLOAD_COMPLETE = "uploadComplete";
    private static $STATUS_NOT_COMPLETE = 0;
    private static $STATUS_COMPLETE = 1;
    private static $PARAM_ACTION = "action";
    private static $PARAM_FILE_ID = "fileId";
    private static $PARAM_FILE_NAME = "fileName";
    private static $PARAM_STREAMLET_NAME = "streamletName";
    private static $PARAM_STREAMLET_NO = "streamletNo";
    private static $PARAM_UPLOADED_FILE = "uploadedFile";
    private static $CONTEXT_TYPE = "contextType";
    private static $CONTEXT_VALUE = "contextValue";
    private static $INTENT="intent";


    public static function execute()
    {
        $action = (isset($_POST[self::$PARAM_ACTION])) ? $_POST[self::$PARAM_ACTION] : null;

        switch ($action) {
            case self::$ACTION_UPLOAD_REQUEST;
                self::uploadRequest();
                break;
            case self::$ACTION_UPLOAD:
                self::upload();
                break;
            case self::$ACTION_UPLOAD_COMPLETE:
                self::uploadComplete();
                break;
            default :
                throw new Exception("Invalid Action.");
                break;
        }
    }

    private static function upload()
    {
        global $config;
        $contextValue = (isset($_POST[self::$CONTEXT_VALUE])) ? $_POST[self::$CONTEXT_VALUE] : null;
        $intent = (isset($_POST[self::$INTENT])) ? $_POST[self::$INTENT] : null;

        //$configGraphsDAO=new ConfigGraphsDAO();
       // $fileId=(isset($_POST[self::$PARAM_FILE_ID])) ? $_POST[self::$PARAM_FILE_ID] : null;
       // if($fileId==null){
        //    throw new Exception("Invaid File Id. Pass a valid file Id");
        //}

        //$configGraph=$configGraphsDAO->findById($fileId);

        //if($configGraph==null){
          //  throw new Exception("Invalid File Id. Could not retrieve graph from database ");
        //}


       //TODO check for status completion here

        $contextInfo=new ContextInfo();
        //$contextInfo->setConfigGraphID($fileId);
        $contextInfo->setContextInfo($contextValue);
        $contextInfo->setIntent($intent);

        $contextInfoDAO= new ContextInfoDAO();
        $contextInfoDAO->insertContextInfo($contextInfo);

        $myfile = fopen("testfile.txt", "a");
        //$contextType=(isset($_POST[self::$CONTEXT_TYPE])) ? $_POST[self::$CONTEXT_TYPE] : null;
        // fwrite($myfile,$contextType );
        fwrite($myfile, $intent);
        fwrite($myfile, "\n");
        fwrite($myfile, $contextValue);
        fwrite($myfile, "\n");
        fclose($myfile);


    }

    private static function uploadComplete()
    {
        //TODO: I might want to implment this when the contex snapshot is large and requires updating several tables
        global $config;

    }

    private static function uploadRequest()
    {
        $configGraphDAO = new ConfigGraphsDAO();
        $fileName = (isset($_POST[self::$PARAM_FILE_NAME])) ? $_POST[self::$PARAM_FILE_NAME] : null;
        $configGraph = new ConfigGraph();
        $configGraph->setIsNew(1);
        $fileId = $configGraphDAO->insertFile($configGraph);

        echo self::$PARAM_FILE_ID . "=" . $fileId;

    }

}

?>
