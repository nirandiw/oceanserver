<?php
/**
 * Created by IntelliJ IDEA.
 * User: workshop
 * Date: 3/12/2014
 * Time: 4:40 PM
 */

class ContextInfoDAO extends DAO {
    public function insertContextInfo($contextInfo) {
        $sql = "INSERT INTO contextinfo(CONTEXT_INFO, INTENT) " .
            "VALUES('{$contextInfo->getContextInfo()}', '{$contextInfo->getIntent()}')";
        return $this->insert($sql);
    }

    public function findContextSnapShotsByFileId($fileId) {
        $sql = "SELECT * FROM contextinfo WHERE CONFIG_GRAPH_ID = $fileId";
        $result = $this->query($sql);
        $contextInfoList = array();
        while($row = mysql_fetch_array($result)) {
            $contextInfo = $this->getStreamlet($row);
            array_push($contextInfoList, $contextInfo);
        }
        return $contextInfoList;
    }

    public function getStreamlet($row) {
        $streamlet = new Streamlet();
        $streamlet->setFileId($row['file_id']);
        $streamlet->setStreamletName($row['streamlet_name']);
        $streamlet->setStreamletNo($row['streamlet_no']);
        return $streamlet;
    }
}



?>