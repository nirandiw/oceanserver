<?php
/* 
 * Uploading page
 */
require_once 'UploadController.php';

try {
    UploadController::execute();
} catch (Exception $e) {
    header("HTTP/1.1 500 Internal Server Error");
    echo "Error Message : " . $e->getMessage();
}
?>
